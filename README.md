# &lt;cells-lv-card-description&gt;

  The component allows you to display a credit account, the account number, what type of credit it is, 
  the name of the company, the real currency, the available balance, the available balance and the 
  authorized balance.

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
Example:
```html
<cells-lv-card-description></cells-lv-card-description>
```

## Styling
  The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                    | Selector | CSS Property | Value      |
| ---------------------------------- | -------- | ------------ | ---------- |
| --cells-fontDefault                | :host    | font-family  | sans-serif |
| --cells-bfm-subheader-card-balance | .balance | color        | --bbva-500 |
### @apply
| Mixins                                      | Selector         | Value |
| ------------------------------------------- | ---------------- | ----- |
| --cells-lv-card-description                 | :host            | {}    |
| --cells-lv-card-description-content         | .content         | {}    |
| --cells-lv-card-description-card            | .card            | {}    |
| --cells-lv-card-description-left-container  | .left-container  | {}    |
| --cells-lv-card-description-richt-container | .right-container | {}    |
| --cells-lv-card-description-imgContainer    | .imgContainer    | {}    |
| --cells-lv-card-description-imgView         | .imgView         | {}    |
| --cells-lv-card-description-contrato        | .contrato        | {}    |
| --cells-lv-card-description-title           | .title           | {}    |
| --cells-lv-card-description-accountType     | .accountType     | {}    |
| --cells-lv-card-description-period          | .period          | {}    |
| --cells-lv-card-description-balances        | .balances        | {}    |
| --cells-lv-card-description-balance         | .balance         | {}    |
| --cells-lv-card-description-balanceQuantity | .balanceQuantity | {}    |
