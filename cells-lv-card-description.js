{
  const {
    html,
  } = Polymer;
  /**
    `<cells-lv-card-description>` Description.
    The component allows you to display a credit account, the account number, what type of credit it is, 
    the name of the company, the real currency, the available balance, the available balance and the 
    authorized balance.

    Example:

    ```html
    <cells-lv-card-description></cells-lv-card-description>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-lv-card-description                 | :host            | {}    |
    | --cells-lv-card-description-content         | .content         | {}    |
    | --cells-lv-card-description-card            | .card            | {}    |
    | --cells-lv-card-description-left-container  | .left-container  | {}    |
    | --cells-lv-card-description-richt-container | .right-container | {}    |
    | --cells-lv-card-description-imgContainer    | .imgContainer    | {}    |
    | --cells-lv-card-description-imgView         | .imgView         | {}    |
    | --cells-lv-card-description-contrato        | .contrato        | {}    |
    | --cells-lv-card-description-title           | .title           | {}    |
    | --cells-lv-card-description-accountType     | .accountType     | {}    |
    | --cells-lv-card-description-period          | .period          | {}    |
    | --cells-lv-card-description-balances        | .balances        | {}    |
    | --cells-lv-card-description-balance         | .balance         | {}    |
    | --cells-lv-card-description-balanceQuantity | .balanceQuantity | {}    |

    * @customElement cells-lv-card-description
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLvCardDescription extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-lv-card-description';
    }

    static get properties() {
      return {
        /**
         * Assign account number in bold letters
         */
        title: String,
        /**
         * Allows to assign the type of account
         */
        accountType: String,
        /**
         * Allows you to assign a type of credit
         */
        creditType: String,
        /**
        * Assign company title
        */
        company: String,
        /**
        * Allows you to assign a logo image to the component
        */
        img: String,
        /**
        * Allows to add an array of properties, for balances
        */
        balancesItems: {
          type: Array,
          value: () => ([])
        }
      };
    }

    _isEqualTo(option, value) {
      return option === value;
    }
    _isEmpty(firstValue, secondValue) {
      return ((firstValue !== '' && firstValue !== undefined) && (secondValue !== '' && secondValue !== undefined));
    }
  }

  customElements.define(CellsLvCardDescription.is, CellsLvCardDescription);
}
